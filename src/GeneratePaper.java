

import java.awt.Color;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class GeneratePaper extends javax.swing.JPanel {
    ResultSet rs;
    ResultSet sr;
    Connection conn;
    MainFrame obj;
//    public GeneratePaper(){
//        
//    }
    public GeneratePaper(MainFrame ref) {
//        System.out.println("inside Add Generate Paper");
        initComponents();
        obj=ref;
        try{      
            conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/questionsdb","chintan","chintan");
            String str="";
            Statement statements=conn.createStatement();
            rs=statements.executeQuery("SELECT * FROM subject");
            while(rs.next()){
//              str+=rs.getString("sub_name");
                txtMarks.setText(str);
                cmbSubject.addItem(rs.getString("sub_name"));
            }
        }catch(SQLException e){}
            fillSubject();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        ContainPane = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cmbSubject = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtMarks = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        cmbDifficulty = new javax.swing.JComboBox<>();
        jButton1 = new javax.swing.JButton();
        ChapterName = new java.awt.List();
        jSeparator1 = new javax.swing.JSeparator();

        setBackground(new java.awt.Color(204, 255, 255));

        ContainPane.setBackground(new java.awt.Color(204, 255, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Select Subject");

        cmbSubject.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbSubjectItemStateChanged(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Select Chapters");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Enter Total Marks");

        txtMarks.setBackground(new java.awt.Color(204, 255, 255));
        txtMarks.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txtMarks.setForeground(new java.awt.Color(51, 0, 51));
        txtMarks.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtMarks.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        txtMarks.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtMarksFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtMarksFocusLost(evt);
            }
        });
        txtMarks.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMarksActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setText("Select Difficulty Level");

        cmbDifficulty.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Easy", "Medium", "Hard" }));

        jButton1.setText("Generate Paper");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        ChapterName.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        ChapterName.setMultipleMode(true);

        jSeparator1.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout ContainPaneLayout = new javax.swing.GroupLayout(ContainPane);
        ContainPane.setLayout(ContainPaneLayout);
        ContainPaneLayout.setHorizontalGroup(
            ContainPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ContainPaneLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ContainPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ContainPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 236, Short.MAX_VALUE)
                    .addComponent(cmbDifficulty, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cmbSubject, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(ChapterName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtMarks)
                    .addComponent(jSeparator1))
                .addGap(103, 103, 103))
        );
        ContainPaneLayout.setVerticalGroup(
            ContainPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ContainPaneLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ContainPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbSubject, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ContainPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ContainPaneLayout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(111, 111, 111)
                        .addGroup(ContainPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtMarks, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(ContainPaneLayout.createSequentialGroup()
                        .addComponent(ChapterName, javax.swing.GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE)
                        .addGap(50, 50, 50)))
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(ContainPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbDifficulty, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(183, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(ContainPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(ContainPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        String ChaptersNames[]=ChapterName.getSelectedItems();
        for(String str:ChaptersNames){
            System.out.println("chapters: "+str);
        }
        if(ChaptersNames.length>0){
            int marks;
            marks=Integer.parseInt(txtMarks.getText());     
            int diff=getDifficulty();

            obj.showPaper(marks,ChaptersNames,diff);//,qp);
        }
    }//GEN-LAST:event_jButton1ActionPerformed
    private int getDifficulty(){
        int difficulty=1;
        String diff=(String)cmbDifficulty.getSelectedItem();
        if("Easy".equals(diff))
            difficulty=1;
        else if("Medium".equals(diff))
            difficulty=2;
        else if("Hard".equals(diff))
            difficulty=3;
        return difficulty;
    }
    private void cmbSubjectItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbSubjectItemStateChanged
        // TODO add your handling code here:
        String str=(String)cmbSubject.getSelectedItem();
//      System.out.println(str);
        try{
            ChapterName.clear();
            Statement statement=conn.createStatement();
            rs=statement.executeQuery("SELECT * FROM subject WHERE sub_name='"+str+"'");
            rs.next();
            int sub_code=rs.getInt("sub_code");
//            System.out.println(sub_code+"");
//            txtMarks.setText(sub_code+""); 
//            statement=conn.createStatement();
//            int sub_code=17513;
            sr=statement.executeQuery("SELECT * FROM chapter WHERE sub_code="+sub_code);
            while(sr.next()){
//                cmbChapter.addItem(sr.getString("ch_name"));
                ChapterName.add(sr.getString("ch_name"));
            }
//            txtMarks.setText(str);
        }catch(SQLException e){
            System.out.println("exception :"+e.getMessage());
        }
    }//GEN-LAST:event_cmbSubjectItemStateChanged

    private void txtMarksFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtMarksFocusGained
        // TODO add your handling code here:
//        txtMarks.setBackground(Color.white);
    }//GEN-LAST:event_txtMarksFocusGained

    private void txtMarksFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtMarksFocusLost
        // TODO add your handling code here:
//        txtMarks.setBackground(new Color(204,255,255));
    }//GEN-LAST:event_txtMarksFocusLost

    private void txtMarksActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMarksActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMarksActionPerformed

    private void fillSubject(){
        try{
            String str="";
            Statement statementss=conn.createStatement();
            rs=statementss.executeQuery("SELECT * FROM subject");
            while(rs.next()){
                str+=rs.getString("sub_name");
//                txtMarks.setText(str);
                cmbSubject.addItem(rs.getString("sub_name"));
            }
        }
        catch(SQLException e){}
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private java.awt.List ChapterName;
    private javax.swing.JPanel ContainPane;
    private javax.swing.JComboBox<String> cmbDifficulty;
    private javax.swing.JComboBox<String> cmbSubject;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField txtMarks;
    // End of variables declaration//GEN-END:variables
}
