/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author vijay
 */
import java.io.FileOutputStream;
import java.util.Date;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.beans.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PDFDemos {
    private final String FILE = "C:/advance java/qpgenerator/src/chintan.pdf";
    private Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,
            Font.BOLD);
    private Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.NORMAL, BaseColor.RED);
    private Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16,
            Font.BOLD);
    private Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.BOLD);
    int subjectCode;
    int[] questionsID;
    int marks;
    Connection conn;
    PDFDemos(int subjectCode,int[] questionsID,int marks){
        try {
            this.subjectCode=subjectCode;
            this.questionsID=questionsID;
            this.marks=marks;
            conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/questionsdb","chintan","chintan");
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
            addMetaData(document);
            addTitlePage(document);
            addContent(document);
            System.out.println("pdf generated!!!!");
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void addMetaData(Document document) {
        document.addTitle("My first PDF");
        document.addSubject("Using iText");
        document.addKeywords("Java, PDF, iText");
        document.addAuthor("Lars Vogel");
        document.addCreator("Lars Vogel");
    }

    private void addTitlePage(Document document)
            throws DocumentException {
        Paragraph preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph(subjectCode+"", catFont));
        if(marks>80){
            preface.add(new Paragraph("hours:3", smallBold));
        }else if(marks<80 && marks>30){
            preface.add(new Paragraph("hours:2", smallBold));
        }else {
            preface.add(new Paragraph("hours:1:30", smallBold));
        }      
        preface.add(new Paragraph("_______________________________________________________________________________________", smallBold));
        addEmptyLine(preface,1);
        document.add(preface);
    }

    private void addContent(Document document) throws DocumentException {
        Paragraph paragraph=new Paragraph();
        createTable(paragraph);
        document.add(paragraph);
    }

    private void createTable(Paragraph paragraph)
            throws BadElementException {
        try{
            int ctmarks=0;
            int cmarks=0;
            PdfPTable table = new PdfPTable(3);

            ResultSet rs;
            java.sql.Statement statement=conn.createStatement();
            PdfPCell c1 = new PdfPCell(new Phrase("No."));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("Question"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("Marks"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
            table.setHeaderRows(1);
            int i=0;
            for(int id:questionsID){
                if(id==0){
                    break;
                }
                rs=statement.executeQuery("SELECT * FROM questions WHERE id="+id);
                while(rs.next()){
                    table.addCell(i+"");
                    table.addCell(rs.getString("question"));
                    ctmarks+=rs.getInt("marks");
                    cmarks=rs.getInt("marks");
                    if(ctmarks>marks){
                        cmarks-=(ctmarks-marks);
                        table.addCell(cmarks+"");
                    }
                    else 
                        table.addCell(cmarks+"");
                    i++;                 
                }
            }
            paragraph.add(table);
        }catch(SQLException e){System.out.println(e);}
    }

    private void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
    public static void main(String args[]){
        int id[]={11,12,13,14};
        new PDFDemos(17513,id,4);
    }
}
