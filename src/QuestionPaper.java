
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author MAHESH
 */
public class QuestionPaper extends javax.swing.JPanel {

    Connection conn;
    HashSet<Question> questions;
    PreparedStatement ps;
    int[] id;
    int currentMarks=0,Tmarks,difficulty;
    String[] chapters;
    int[] chID=new int[10];
    MainFrame obj;
    int qmarks=0;
    int MinProbability=0;
    int CurrentProbability=0;
    boolean b;
    int subjectCode;
    int HashSetSize=0;
    int sizeCounter=0;
    public QuestionPaper(MainFrame ref){
        obj=ref;
    };
    public QuestionPaper(int marks,String[] chapter,int diff,MainFrame ref) {
        initComponents();
        obj=ref;
//        System.out.println("Question paper :::");
//        System.out.println("marks :"+marks);
//        System.out.println("diff"+diff);
        
        try{
            questions=new HashSet<>();
            conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/questionsdb","chintan","chintan");
            ContainerPane.setLayout(new GridLayout(0,1));
        }catch(SQLException e){System.out.println("Exception in paper generate "+e);}
        this.Tmarks=marks;
        this.difficulty=diff;
        this.chapters=chapter;
        try{
            setChapterId();
            b=addQuestions();
            generateQuestion();
        }catch(SQLException e){
            System.out.println("Error "+e);
        }
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        ContainerPane = new javax.swing.JPanel();

        setBackground(new java.awt.Color(204, 255, 255));

        jPanel1.setBackground(new java.awt.Color(204, 255, 255));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/reply-all-button.png"))); // NOI18N
        jLabel1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel1MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(454, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        ContainerPane.setBackground(new java.awt.Color(204, 255, 255));

        javax.swing.GroupLayout ContainerPaneLayout = new javax.swing.GroupLayout(ContainerPane);
        ContainerPane.setLayout(ContainerPaneLayout);
        ContainerPaneLayout.setHorizontalGroup(
            ContainerPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 510, Short.MAX_VALUE)
        );
        ContainerPaneLayout.setVerticalGroup(
            ContainerPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 408, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(ContainerPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
                .addComponent(ContainerPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(56, 56, 56))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseClicked
        obj.showGeneratePap();
    }//GEN-LAST:event_jLabel1MouseClicked
    private void generateQuestion(){
        try{
            String strr;
            Statement statement=conn.createStatement();
            int diff;
            int mks;
            int prob;
            int cid;
            HashSetSize=0;
            System.out.println("Current probability ="+CurrentProbability);
//            for(Question que:questions){
//                ResultSet sr=statement.executeQuery("select * from questions WHERE question='"+que.getQuestion()+"'");
//                sr.next();
//                System.out.println("question :"+que.getQuestion()+"------"+sr.getInt("level"));
//            }
            if(b){
                //if all questions are
                for(Question quest:this.questions){
                    HashSetSize++;
                    if(qmarks==Tmarks){
                        setProbability();
                        new PDFDemos(subjectCode,id,Tmarks);
                        break;
                    }
                    strr=quest.getQuestion();
                    ResultSet sr=statement.executeQuery("select * from questions WHERE question='"+quest.getQuestion()+"'");
                    sr.next();
                    if(sr.getInt("probability")==CurrentProbability){
                        System.out.println("probability= "+sr.getInt("probability")+"CurrentProbability= "+CurrentProbability);
                        qmarks+=sr.getInt("marks");
                        if(qmarks>Tmarks){
                            diff=qmarks-Tmarks;
                            mks=sr.getInt("marks")-diff;
                            ContainerPane.add(new Label(strr+"   "+mks+".M"));
                            id[sizeCounter]=sr.getInt("id");
                            sizeCounter++;
                            prob=sr.getInt("probability");
                            cid=sr.getInt("id");
//                            System.out.println(statement.execute("UPDATE questions set probability="+prob+" WHERE id="+cid));
                            new PDFDemos(subjectCode,id,Tmarks);
                            setProbability();
                            break;
                        }
                        ContainerPane.add(new Label(strr+"   "+sr.getInt("marks")+".M"));
                        id[sizeCounter]=sr.getInt("id");
                        sizeCounter++;
                        prob=sr.getInt("probability");
                        cid=sr.getInt("id");
                        prob++;
//                        System.out.println(statement.execute("UPDATE questions set probability="+prob+" WHERE id="+cid));
                    }
                    if(HashSetSize==questions.size() && qmarks<Tmarks){
                        System.out.println("current probability increased ");
                        CurrentProbability++;
                        generateQuestion();
                    }
                }
            }
            else{
                 JOptionPane.showMessageDialog(this,"Not Enough questions to generate question paper");
            }
        }catch(SQLException e){
            System.out.println("in generate paper "+e);
        }        
    }
    private void displayData(String str){
        try{
            System.out.println("---------------------------displaying table data---------------------------");
            Statement statement=conn.createStatement();
            ResultSet rs;
            rs=statement.executeQuery("SELECT * FROM questions WHERE question='"+str+"'");
            rs.next();
            System.out.println("chapter ID ="+rs.getInt("chapter_id"));
            int id=rs.getInt("id");
            rs=statement.executeQuery("SELECT * from chapter where ch_id="+id);
            rs.next();
            System.out.println("Subject code is "+rs.getInt("sub_code"));
            int sid=rs.getInt("sub_code");
            rs=statement.executeQuery("SELECT * from subject where sub_id="+sid);
            rs.next();
            System.out.println("subject name: "+rs.getInt("sub_name"));
        }catch(SQLException s){
            System.out.println("exception "+s);
        }
    }
    private void setProbability(){
  	try{
            int prob;
            Statement statement=conn.createStatement();
            ResultSet rset;
            String str="UPDATE questions set probability=? WHERE id=?";
            ps = conn.prepareStatement(str);
//            System.out.println("inside probability");
//            for(int cid:this.id){
//            System.out.println("id="+cid);
//            }
            for(int cid:this.id){
                if(cid==0)
                    break;
                rset=statement.executeQuery("SELECT * FROM questions WHERE id="+cid);
                rset.next();
                prob=rset.getInt("level");
                prob++;
                ps.setInt(1,prob);
                ps.setInt(2,cid);
                ps.execute();
            }
  	}catch(SQLException e){System.out.println("exception "+e);}
    }
    private void setChapterId() throws SQLException{
        try{
            ResultSet res;
            Statement stat=conn.createStatement();
            int i=0;
            for(String str:chapters){
                res=stat.executeQuery("SELECT * FROM chapter WHERE ch_name='"+str+"'");
                res.next();
                int id=res.getInt("ch_id");
                if(i==0){
                    subjectCode=res.getInt("sub_code");
                }   
                chID[i]=id;
                i++;
                System.out.println("i="+i);
            }
            
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
    }
    private boolean addQuestions() throws SQLException{
        try{
            ResultSet res;
//            System.out.println("inside add questions");
            Question q;
            Statement stat=conn.createStatement();
            int diff=difficulty-1;
            int i=0;
            for(int id:chID){
                System.out.println(id+"");
            }
//            for(int j=0;j<3;j++){
//                System.out.println("==================================difficulty "+difficulty);
//                if(difficulty==0){
//                    break;
//                }
                for(int ID:chID){
                    if(ID==0){
                        break;
                    }
                    res=stat.executeQuery("SELECT * FROM questions WHERE chapter_id="+ID+" AND level="+difficulty+" order by probability desc");
                    if(res.next()){
                        
                        do{
                            System.out.println("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-"+res.getString("question")+"");
                            if(checkQuest(res.getString("question"))){
                                q=new Question();
                                q.setQuestion(res.getString("question"));
                                questions.add(q);
                                currentMarks+=res.getInt("marks");
                            }
                        }while(res.next());
                    }
                }
//                difficulty--;
            if(currentMarks<Tmarks){
                return false;
            }
            id=new int[questions.size()];
            getMinimumPriority();
        }catch(SQLException e){
            System.out.println("excepton in add Questions: "+e.getMessage());
        }
        return true;
    }
    private void getMinimumPriority(){
        try{
            ResultSet rs;
            Statement statement=conn.createStatement();
            System.out.println("Inside min probability");
            int i=0;
            for(Question question:this.questions){
                rs=statement.executeQuery("SELECT * FROM questions WHERE question='"+question.getQuestion()+"'");
                rs.next();
                if(i==0){
                    MinProbability=rs.getInt("probability");
                    CurrentProbability=MinProbability;
                }
                else if(MinProbability>rs.getInt("probability")){
                    MinProbability=rs.getInt("probability");
                    CurrentProbability=MinProbability;
                }
                i++;
            }
            
            System.out.println("Current prob:"+CurrentProbability);
            System.out.println("minimum prob:"+MinProbability);
        }catch(SQLException e){System.out.println(e.getMessage());}
    }
    private boolean checkQuest(String str){
        for(Question que:questions){
          if(str.equals(que.getQuestion())){
            return false;
          }
        }
        return true;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel ContainerPane;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
