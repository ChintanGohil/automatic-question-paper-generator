
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
public class MainFrame extends javax.swing.JFrame {

    GeneratePaper gpane;
    QuestionPaper qp;//=new QuestionPaper(this);
//    GeneratePaper gp=new GeneratePaper(this);
    boolean addQuestion=false;
    boolean generatePaper=false;
    boolean addChapter=true;
    boolean addSubject=false;
    boolean questionPaper=false;
    AddSubjectPane asp=new AddSubjectPane();
    AddChapterPane acp=new AddChapterPane();
    AddQuestionPane aqp=new AddQuestionPane();
    public MainFrame() {
//        System.out.println("inside main");
        initComponents();
//        System.out.println("before adding");
        Dimension dimension=Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(dimension.width/2-getWidth()/2,dimension.height/2-getHeight()/2);
        ContainerPane.add(acp);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        AddSubjectpane = new javax.swing.JPanel();
        AddSubjectlbl = new javax.swing.JLabel();
        AddSubjectsep = new javax.swing.JSeparator();
        AddChapterpane = new javax.swing.JPanel();
        AddChapterlbl = new javax.swing.JLabel();
        AddChaptersep = new javax.swing.JSeparator();
        AddQuestionpane = new javax.swing.JPanel();
        AddQuestionlbl = new javax.swing.JLabel();
        AddQuestionsep = new javax.swing.JSeparator();
        GeneratePaperPane = new javax.swing.JPanel();
        GeneratePaperlbl = new javax.swing.JLabel();
        GeneratePapersep = new javax.swing.JSeparator();
        jPanel9 = new javax.swing.JPanel();
        ClosePane = new javax.swing.JPanel();
        CrossLbl = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        ContainerPane = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(51, 0, 51));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(51, 0, 51));

        jPanel2.setBackground(new java.awt.Color(51, 0, 51));

        jPanel4.setBackground(new java.awt.Color(51, 0, 51));

        AddSubjectpane.setBackground(new java.awt.Color(51, 0, 51));
        AddSubjectpane.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        AddSubjectpane.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                AddSubjectpaneMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                AddSubjectpaneMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                AddSubjectpaneMouseExited(evt);
            }
        });

        AddSubjectlbl.setBackground(new java.awt.Color(204, 204, 255));
        AddSubjectlbl.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        AddSubjectlbl.setForeground(new java.awt.Color(204, 204, 255));
        AddSubjectlbl.setText("Add Subject");

        AddSubjectsep.setBackground(new java.awt.Color(51, 0, 51));
        AddSubjectsep.setForeground(new java.awt.Color(51, 0, 51));

        javax.swing.GroupLayout AddSubjectpaneLayout = new javax.swing.GroupLayout(AddSubjectpane);
        AddSubjectpane.setLayout(AddSubjectpaneLayout);
        AddSubjectpaneLayout.setHorizontalGroup(
            AddSubjectpaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AddSubjectpaneLayout.createSequentialGroup()
                .addGroup(AddSubjectpaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(AddSubjectpaneLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(AddSubjectlbl, javax.swing.GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE))
                    .addComponent(AddSubjectsep))
                .addContainerGap())
        );
        AddSubjectpaneLayout.setVerticalGroup(
            AddSubjectpaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AddSubjectpaneLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(AddSubjectlbl)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                .addComponent(AddSubjectsep, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        AddChapterpane.setBackground(new java.awt.Color(51, 0, 51));
        AddChapterpane.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        AddChapterpane.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                AddChapterpaneMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                AddChapterpaneMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                AddChapterpaneMouseExited(evt);
            }
        });

        AddChapterlbl.setBackground(new java.awt.Color(204, 204, 255));
        AddChapterlbl.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        AddChapterlbl.setForeground(new java.awt.Color(204, 204, 255));
        AddChapterlbl.setText("Add Chapter");

        AddChaptersep.setBackground(new java.awt.Color(51, 0, 51));
        AddChaptersep.setForeground(new java.awt.Color(51, 0, 51));

        javax.swing.GroupLayout AddChapterpaneLayout = new javax.swing.GroupLayout(AddChapterpane);
        AddChapterpane.setLayout(AddChapterpaneLayout);
        AddChapterpaneLayout.setHorizontalGroup(
            AddChapterpaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AddChapterpaneLayout.createSequentialGroup()
                .addGroup(AddChapterpaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, AddChapterpaneLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(AddChapterlbl, javax.swing.GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE))
                    .addComponent(AddChaptersep))
                .addContainerGap())
        );
        AddChapterpaneLayout.setVerticalGroup(
            AddChapterpaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AddChapterpaneLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(AddChapterlbl)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(AddChaptersep, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        AddQuestionpane.setBackground(new java.awt.Color(51, 0, 51));
        AddQuestionpane.setForeground(new java.awt.Color(204, 255, 255));
        AddQuestionpane.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        AddQuestionpane.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                AddQuestionpaneMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                AddQuestionpaneMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                AddQuestionpaneMouseExited(evt);
            }
        });

        AddQuestionlbl.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        AddQuestionlbl.setForeground(new java.awt.Color(204, 204, 255));
        AddQuestionlbl.setText("Add Question");

        AddQuestionsep.setBackground(new java.awt.Color(51, 0, 51));
        AddQuestionsep.setForeground(new java.awt.Color(51, 0, 51));

        javax.swing.GroupLayout AddQuestionpaneLayout = new javax.swing.GroupLayout(AddQuestionpane);
        AddQuestionpane.setLayout(AddQuestionpaneLayout);
        AddQuestionpaneLayout.setHorizontalGroup(
            AddQuestionpaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AddQuestionpaneLayout.createSequentialGroup()
                .addGroup(AddQuestionpaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(AddQuestionpaneLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(AddQuestionlbl, javax.swing.GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE))
                    .addComponent(AddQuestionsep))
                .addContainerGap())
        );
        AddQuestionpaneLayout.setVerticalGroup(
            AddQuestionpaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AddQuestionpaneLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(AddQuestionlbl, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(AddQuestionsep, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        GeneratePaperPane.setBackground(new java.awt.Color(51, 0, 51));
        GeneratePaperPane.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        GeneratePaperPane.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                GeneratePaperPaneMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                GeneratePaperPaneMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                GeneratePaperPaneMouseExited(evt);
            }
        });

        GeneratePaperlbl.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        GeneratePaperlbl.setForeground(new java.awt.Color(204, 204, 255));
        GeneratePaperlbl.setText("Generate Paper");

        GeneratePapersep.setBackground(new java.awt.Color(51, 0, 51));
        GeneratePapersep.setForeground(new java.awt.Color(51, 0, 51));

        javax.swing.GroupLayout GeneratePaperPaneLayout = new javax.swing.GroupLayout(GeneratePaperPane);
        GeneratePaperPane.setLayout(GeneratePaperPaneLayout);
        GeneratePaperPaneLayout.setHorizontalGroup(
            GeneratePaperPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(GeneratePaperPaneLayout.createSequentialGroup()
                .addGroup(GeneratePaperPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(GeneratePaperPaneLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(GeneratePaperlbl, javax.swing.GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE))
                    .addComponent(GeneratePapersep))
                .addContainerGap())
        );
        GeneratePaperPaneLayout.setVerticalGroup(
            GeneratePaperPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(GeneratePaperPaneLayout.createSequentialGroup()
                .addGap(0, 13, Short.MAX_VALUE)
                .addComponent(GeneratePaperlbl)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(GeneratePapersep, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(AddSubjectpane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(AddChapterpane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(AddQuestionpane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(GeneratePaperPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(AddSubjectpane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                .addComponent(AddChapterpane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(AddQuestionpane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(GeneratePaperPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(266, Short.MAX_VALUE))
        );

        jPanel9.setBackground(new java.awt.Color(51, 0, 51));

        ClosePane.setBackground(new java.awt.Color(51, 0, 51));
        ClosePane.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        ClosePane.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ClosePaneMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                ClosePaneMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                ClosePaneMouseExited(evt);
            }
        });

        CrossLbl.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        CrossLbl.setForeground(new java.awt.Color(255, 255, 255));
        CrossLbl.setText("X");
        ClosePane.add(CrossLbl);

        jPanel10.setBackground(new java.awt.Color(51, 0, 51));

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(204, 255, 255));
        jLabel5.setText("Question Paper Generator");

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 456, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(ClosePane, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(ClosePane, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel10, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        ContainerPane.setBackground(new java.awt.Color(204, 255, 255));

        jSeparator1.setBackground(new java.awt.Color(204, 255, 255));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jSeparator1)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addComponent(ContainerPane, javax.swing.GroupLayout.DEFAULT_SIZE, 632, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addComponent(ContainerPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void ClosePaneMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ClosePaneMouseEntered
        // TODO add your handling code here:
        ClosePane.setBackground(Color.black);
        CrossLbl.setForeground(Color.white);
    }//GEN-LAST:event_ClosePaneMouseEntered
    public void showPaper(int marks,String[] chapter,int diff){//,QuestionPaper qp){
       
//        this.qp=qp;
        ContainerPane.remove(gpane);
        qp=new QuestionPaper(marks,chapter,diff,this);
        ContainerPane.add(qp);
        ContainerPane.updateUI();
    }
    public void showGeneratePap(){
        ContainerPane.remove(qp);
        ContainerPane.add(gpane);
        ContainerPane.updateUI();
    }
    private void ClosePaneMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ClosePaneMouseClicked
        // TODO add your handling code here:
         System.exit(0);
    }//GEN-LAST:event_ClosePaneMouseClicked

    private void ClosePaneMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ClosePaneMouseExited
        // TODO add your handling code here:
        ClosePane.setBackground(new Color(51,0,51));
    }//GEN-LAST:event_ClosePaneMouseExited

    private void AddQuestionpaneMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AddQuestionpaneMouseEntered
        // TODO add your handling code here:
//        new Thread(new Runnable(){
//            @Override
//            public void run(){
                AddQuestionlbl.setForeground(new Color(255,255,255));
                AddQuestionsep.setBackground(new Color(204,255,255));
//                while(AddQuestionsep.getWidth()>0){
////                    System.out.println(AddSubjectsep.getWidth());
//                    continue;
//                }
//                while(AddQuestionsep.getWidth()<220){
//                    AddSubjectsep.setBounds(AddQuestionsep.getX(),AddQuestionsep.getY(),AddQuestionsep.getWidth()+10,AddQuestionsep.getHeight());
//                    try{
//                        Thread.sleep(5);
//                    }catch(InterruptedException e){
//                        System.out.println("exception "+e);
//                    }
//                }
//            }
//        }).start();
    }//GEN-LAST:event_AddQuestionpaneMouseEntered

    private void AddSubjectpaneMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AddSubjectpaneMouseEntered
        // TODO add your handling code here:
//        new Thread(new Runnable(){
//            @Override
//            public void run(){
                AddSubjectlbl.setForeground(new Color(255,255,255));
                AddSubjectsep.setBackground(new Color(204,255,255));
//                while(AddSubjectsep.getWidth()>0){
////                    System.out.println(AddSubjectsep.getWidth());
//                    continue;
//                }
//                while(AddSubjectsep.getWidth()<220){
//                    AddSubjectsep.setBounds(AddSubjectsep.getX(),AddSubjectsep.getY(),AddSubjectsep.getWidth()+10,AddSubjectsep.getHeight());
//                    try{
//                        Thread.sleep(5);
//                    }catch(InterruptedException e){
//                        System.out.println("exception "+e);
//                    }
//                }
//            }
//        }).start();
    }//GEN-LAST:event_AddSubjectpaneMouseEntered

    private void AddSubjectpaneMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AddSubjectpaneMouseExited
        // TODO add your handling code here:
//        new Thread(new Runnable(){
//            @Override
//            public void run(){
                AddSubjectlbl.setForeground(new Color(204,204,255));
//              AddSubjectsep.setBackground(new Color(204,255,255));  
//                while(AddSubjectsep.getWidth()<220){
////                    System.out.println(AddSubjectsep.getWidth());
//                    continue;
//                }
//                while(AddSubjectsep.getWidth()>0){
//                    AddSubjectsep.setBounds(AddSubjectsep.getX(),AddSubjectsep.getY(),AddSubjectsep.getWidth()-10,AddSubjectsep.getHeight());
//                    try{
//                        Thread.sleep(5);
//                    }catch(InterruptedException e){
//                        System.out.println("exception "+e);
//                    }
//                }
//                AddSubjectsep.setBounds(AddSubjectsep.getX(),AddSubjectsep.getY(),0,AddSubjectsep.getHeight());
                AddSubjectsep.setBackground(new Color(51,0,51));
//            }
//        }).start();
    }//GEN-LAST:event_AddSubjectpaneMouseExited

    private void AddChapterpaneMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AddChapterpaneMouseClicked
        // TODO add your handling code here:
//        if(!addChapter){
//            removePane();
            ContainerPane.removeAll();
//            acp=new AddChapterPane();
            ContainerPane.add(acp);
            ContainerPane.updateUI();
//            addChapter=true;
//        }        
    }//GEN-LAST:event_AddChapterpaneMouseClicked
    
    private void AddChapterpaneMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AddChapterpaneMouseEntered
        // TODO add your handling code here:
//        new Thread(new Runnable(){
//            @Override
//            public void run(){
                AddChapterlbl.setForeground(new Color(255,255,255));
                AddChaptersep.setBackground(new Color(204,255,255));
//                while(AddSubjectsep.getWidth()>0){
////                    System.out.println(AddSubjectsep.getWidth());
//                    continue;
//                }
//                while(AddChaptersep.getWidth()<220){
//                    AddChaptersep.setBounds(AddChaptersep.getX(),AddChaptersep.getY(),AddChaptersep.getWidth()+10,AddChaptersep.getHeight());
//                    try{
//                        Thread.sleep(5);
//                    }catch(InterruptedException e){
//                        System.out.println("exception "+e);
//                    }
//                }
//            }
//        }).start();
    }//GEN-LAST:event_AddChapterpaneMouseEntered

    private void AddChapterpaneMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AddChapterpaneMouseExited
        // TODO add your handling code here:
//        new Thread(new Runnable(){
//            @Override
//            public void run(){
               
//              AddSubjectsep.setBackground(new Color(204,255,255));  
//                while(AddChaptersep.getWidth()<220){
//                    System.out.println("waiting");
//                    continue;
//                }
//                while(AddChaptersep.getWidth()>0){
//                    AddChaptersep.setBounds(AddChaptersep.getX(),AddChaptersep.getY(),AddChaptersep.getWidth()-10,AddChaptersep.getHeight());
//                    try{
//                        Thread.sleep(5);
//                    }catch(InterruptedException e){
//                        System.out.println("exception "+e);
//                    }
//                }
//                AddChaptersep.setBounds(AddChaptersep.getX(),AddChaptersep.getY(),0,AddChaptersep.getHeight());
                AddChaptersep.setBackground(new Color(51,0,51));
                AddChapterlbl.setForeground(new Color(204,204,255));
//            }
//        }).start();
    }//GEN-LAST:event_AddChapterpaneMouseExited

    private void AddSubjectpaneMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AddSubjectpaneMouseClicked
        // TODO add your handling code here:
//        if(!addSubject){
//            removePane();
            try{
                System.out.println("before removing all");
                ContainerPane.removeAll();
                System.out.println("after removing all");
                ContainerPane.add(asp);
                ContainerPane.updateUI();
                System.out.println("After Adding ");
            }catch(Exception e){System.out.println("Exception "+e);}
//            addSubject=true;
//        }
    }//GEN-LAST:event_AddSubjectpaneMouseClicked

    private void AddQuestionpaneMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AddQuestionpaneMouseClicked
        // TODO add your handling code here:
//        if(!addChapter){
//            removePane();
            
            ContainerPane.removeAll();
            
            ContainerPane.add(aqp);
            ContainerPane.updateUI();
//            addChapter=true;
//        }
    }//GEN-LAST:event_AddQuestionpaneMouseClicked

    private void GeneratePaperPaneMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_GeneratePaperPaneMouseClicked
        // TODO add your handling code here:
//        if(!generatePaper){
//            removePane();
            gpane=new GeneratePaper(this); 
            ContainerPane.removeAll();
            ContainerPane.add(gpane);
            ContainerPane.updateUI();
//            generatePaper=true;
//        }
    }//GEN-LAST:event_GeneratePaperPaneMouseClicked

    private void AddQuestionpaneMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AddQuestionpaneMouseExited
        // TODO add your handling code here:
//        AddQuestionsep.setBounds(AddQuestionsep.getX(),AddQuestionsep.getY(),0,AddQuestionsep.getHeight());
        AddQuestionsep.setBackground(new Color(51,0,51));
        AddQuestionlbl.setForeground(new Color(204,204,255));
    }//GEN-LAST:event_AddQuestionpaneMouseExited

    private void GeneratePaperPaneMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_GeneratePaperPaneMouseExited
        // TODO add your handling code here:
//        GeneratePapersep.setBounds(GeneratePapersep.getX(),GeneratePapersep.getY(),0,GeneratePapersep.getHeight());
        GeneratePapersep.setBackground(new Color(51,0,51));
        GeneratePaperlbl.setForeground(new Color(204,204,255));
    }//GEN-LAST:event_GeneratePaperPaneMouseExited

    private void GeneratePaperPaneMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_GeneratePaperPaneMouseEntered
        // TODO add your handling code here:
//        new Thread(new Runnable(){
//            @Override
//            public void run(){
                GeneratePaperlbl.setForeground(new Color(255,255,255));
                GeneratePapersep.setBackground(new Color(204,255,255));
//                while(GeneratePapersep.getWidth()>0){
////                    System.out.println(AddSubjectsep.getWidth());
//                    continue;
//                }
//                while(GeneratePapersep.getWidth()<220){
//                    GeneratePapersep.setBounds(GeneratePapersep.getX(),GeneratePapersep.getY(),GeneratePapersep.getWidth()+10,GeneratePapersep.getHeight());
//                    try{
//                        Thread.sleep(5);
//                    }catch(InterruptedException e){
//                        System.out.println("exception "+e);
//                    }
//                }
//            }
//        }).start();
    }//GEN-LAST:event_GeneratePaperPaneMouseEntered
    
//    private void removePane(){
//        if(addQuestion){
//            ContainerPane.remove(aqp);
//            addQuestion=false;
//        }
//        else if(addChapter){
//            ContainerPane.remove(acp);
//            addChapter=false;
//        }
//        else if(addSubject){
//            ContainerPane.remove(asp);
//            addSubject=false;
//        }
//        else if(generatePaper){
//            ContainerPane.remove(gpane);
//            generatePaper=false;
//        }
//        else if(questionPaper){
//            ContainerPane.remove(qp);
//            questionPaper=false;
//        }
//    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            new MainFrame().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel AddChapterlbl;
    private javax.swing.JPanel AddChapterpane;
    private javax.swing.JSeparator AddChaptersep;
    private javax.swing.JLabel AddQuestionlbl;
    private javax.swing.JPanel AddQuestionpane;
    private javax.swing.JSeparator AddQuestionsep;
    private javax.swing.JLabel AddSubjectlbl;
    private javax.swing.JPanel AddSubjectpane;
    private javax.swing.JSeparator AddSubjectsep;
    private javax.swing.JPanel ClosePane;
    private javax.swing.JPanel ContainerPane;
    private javax.swing.JLabel CrossLbl;
    private javax.swing.JPanel GeneratePaperPane;
    private javax.swing.JLabel GeneratePaperlbl;
    private javax.swing.JSeparator GeneratePapersep;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JSeparator jSeparator1;
    // End of variables declaration//GEN-END:variables
}
